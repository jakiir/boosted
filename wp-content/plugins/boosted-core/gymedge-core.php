<?php
/*
Plugin Name: GymEdge Core
Plugin URI: http://radiustheme.com
Description: GymEdge Core Plugin for GymEdge Theme
Version: 1.3.1
Author: Radius Theme
Author URI: http://radiustheme.com
*/

// Text Domain
add_action( 'init', 'gymedge_core_load_textdomain' );
function gymedge_core_load_textdomain() {
	load_plugin_textdomain( 'gymedge-core' , false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}

// Post types
add_action( 'after_setup_theme', 'gymedge_core_post_types', 12 );
function gymedge_core_post_types(){
	if ( !defined( 'GYMEDGE_THEME_VERSION' ) ) {
		return;
	}
	require_once 'radius-posts/rt-posts.php';
	require_once 'radius-posts/rt-postmeta.php';
	require_once 'post-types.php';
	require_once 'post-meta.php';
}

// Visual composer
add_action( 'after_setup_theme', 'gymedge_core_vc_modules', 13 );
function gymedge_core_vc_modules(){
	if ( !defined( 'GYMEDGE_THEME_VERSION' ) || ! defined( 'WPB_VC_VERSION' ) ) {
		return;
	}
	require_once 'vc-modules/inc/abstruct.php';
	require_once 'vc-modules/title.php';
	require_once 'vc-modules/post-slider.php';
	require_once 'vc-modules/trainer.php';
	require_once 'vc-modules/class.php';
	require_once 'vc-modules/schedule.php';
	require_once 'vc-modules/testimonial.php';
	require_once 'vc-modules/info-text.php';
	require_once 'vc-modules/about.php';
	require_once 'vc-modules/pricing-box.php';
	require_once 'vc-modules/counter.php';
	require_once 'vc-modules/gallery.php';
	require_once 'vc-modules/cta-signup.php';
	require_once 'vc-modules/cta-discount.php';
	require_once 'vc-modules/about-fitness.php';
	
	if ( class_exists( 'WooCommerce' ) ) {
		require_once 'vc-modules/product-slider.php';
	}
	
}

// Custom Functions
function rt_vc_pagination(){
	if ( !defined( 'GYMEDGE_THEME_VERSION' ) ) {
		return;
	}
	return GymEdge_Helper::pagination();
}

// Demo Importer settings
require_once 'demo-importer.php';