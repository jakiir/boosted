<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'boosted');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ob6H9[V{Vo=WgOxp(UlUZ4jfE@{>AqC?6M|o!4DNCX}AoECEJ75LN jb<<93+!O&');
define('SECURE_AUTH_KEY',  'o{xOw$RhlDurx5R3}9L@]_Tuy-4Q%WZU951$ISox6OK~<;}[$>D0Ft2V5jI&?/Fu');
define('LOGGED_IN_KEY',    'iae/Le:?L{I6q~4VK#>NupMBoEy{qGE(n +`+U]=62HPnHc4Ya`@yf[h2P:lYi(g');
define('NONCE_KEY',        'YdIxWIZ:>fMD.Kf`3,X5/K=/^0t7{.;+Yk,ZxRMrl>,asOuaylrbzv[1D2TC)`YP');
define('AUTH_SALT',        '=^]i$gu&{oZ`j:Wt7=^?8BV?#F~INSHf_=RcBwDr.T(bWhgC8vSB%MprI>HeM}Sm');
define('SECURE_AUTH_SALT', '{BK2%Tt<VkWdWMw11de4c0MxoZpR).P`.mSO#YMK&i`ZZM7EAMvPi4v83Lwo1sY$');
define('LOGGED_IN_SALT',   'rtTvOX&!E2*ZI.K2>6Fm(KX!zsGp*3aI<t~xy=E._5:RBIL}c#6Yr[$CsGiXdY=m');
define('NONCE_SALT',       '~b_p9}5W6AXK@hX3lW{%{!y/.?.nHQFY9VjeVcoR^b<d2p&oh2bV~1]2|:*sevX|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
