<?php
class GymEdge_VC_Class extends GymEdge_VC_Modules {

	public function __construct(){
		$this->name = __( "GymEdge: Class", 'gymedge-core' );
		$this->base = 'gymedge-vc-class';
		$this->translate = array(
			'title'      => __( "FEATURED CLASSES", 'gymedge-core' ),
		);
		parent::__construct();
	}
	
	public function load_scripts(){
		wp_enqueue_style( 'owl-carousel' );
		wp_enqueue_style( 'owl-theme-default' );
		wp_enqueue_script( 'owl-carousel' );
	}

	public function fields(){
		$fields = array(
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Layout", 'gymedge-core' ),
				"param_name" => "layout",
				'value' => array( 
					'Slider' => 'slider',
					'Grid'   => 'grid',
					),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Slider Style", 'gymedge-core' ),
				"param_name" => "slider_style",
				"value" => array( 
					'Style 1' => 'style1',
					'Style 2' => 'style2',
					),
				'dependency' => array(
					'element' => 'layout',
					'value'   => array( 'slider' ),
					),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => $this->translate['title'],
				"param_name" => "title",
				"value" => __( "FEATURED CLASSES", 'gymedge-core' ),
				'dependency' => array(
					'element' => 'slider_style',
					'value'   => array( 'style1' ),
					),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Trainer and Schedule display", 'gymedge-core' ),
				"param_name" => "info_display",
				"value" => array( 
					'Enabled'  => 'true',
					'Disabled' => 'false',
					),
				'dependency' => array(
					'element' => 'slider_style',
					'value'   => array( 'style2' ),
					),
				"description" => __( "Show or hide trainer and schedule info of next upcoming class. Default: Enabled", 'gymedge-core' ),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Total number of items", 'gymedge-core' ),
				"param_name" => "slider_item_number",
				"value" => 6,
				'dependency' => array(
					'element' => 'layout',
					'value' => array( 'slider' ),
					),
				'description' => __( 'Write -1 to show all', 'gymedge-core' ),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Items Per Page", 'gymedge-core' ),
				"param_name" => "grid_item_number",
				"value" => 9,
				'dependency' => array(
					'element' => 'layout',
					'value' => array( 'grid' ),
					),
				'description' => __( 'Write -1 to show all', 'gymedge-core' ),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Number of columns ( Desktops > 1199px )", 'gymedge-core' ),
				"param_name" => "col_lg",
				"value" => array( 
					'1 col'  => '12',
					'2 col'  => '6',
					'3 col'  => '4',
					'4 col'  => '3',
					'6 col'  => '2',
					),
				"std" => "4",
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Number of columns ( Desktops > 991px )", 'gymedge-core' ),
				"param_name" => "col_md",
				"value" => array( 
					'1 col'  => '12',
					'2 col'  => '6',
					'3 col'  => '4',
					'4 col'  => '3',
					'6 col'  => '2',
					),
				"std" => "4",
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Number of columns ( Tablets > 767px )", 'gymedge-core' ),
				"param_name" => "col_sm",
				"value" => array( 
					'1 col'  => '12',
					'2 col'  => '6',
					'3 col'  => '4',
					'4 col'  => '3',
					'6 col'  => '2',
					),
				"std" => "4",
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Number of columns ( Phones < 768px )", 'gymedge-core' ),
				"param_name" => "col_xs",
				"value" => array( 
					'1 col'  => '12',
					'2 col'  => '6',
					'3 col'  => '4',
					'4 col'  => '3',
					'6 col'  => '2',
					),
				"std" => "6",
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Number of columns ( Small Phones < 480px )", 'gymedge-core' ),
				"param_name" => "col_mobile",
				"value" => array( 
					'1 col'  => '12',
					'2 col'  => '6',
					'3 col'  => '4',
					'4 col'  => '3',
					'6 col'  => '2',
					),
				"std" => "12",
				'dependency' => array(
					'element' => 'layout',
					'value' => array( 'slider' ),
					),
				),
			// Slider options
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Navigation Arrow", 'gymedge-core' ),
				"param_name" => "slider_nav",
				"value" => array( 
					'Enable'  => 'true',
					'Disable' => 'false',
					),
				"description" => __( "Enable or disable navigation arrow. Default: Enable", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Navigation Dots", 'gymedge-core' ),
				"param_name" => "slider_dots",
				"value" => array( 
					'Disable' => 'false',
					'Enable'  => 'true',
					),
				"description" => __( "Enable or disable navigation dots. Default: Disable", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Autoplay", 'gymedge-core' ),
				"param_name" => "slider_autoplay",
				"value" => array( 
					'Enable'  => 'true',
					'Disable' => 'false',
					),
				"description" => __( "Enable or disable autoplay. Default: Enable", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Stop on Hover", 'gymedge-core' ),
				"param_name" => "slider_stop_on_hover",
				"value" => array( 
					'Enable'  => 'true',
					'Disable' => 'false',
					),
				'dependency' => array(
					'element' => 'slider_autoplay',
					'value'   => array( 'true' ),
					),
				"description" => __( "Stop autoplay on mouse hover. Default: Enable", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Autoplay Interval", 'gymedge-core' ),
				"param_name" => "slider_interval",
				"value" => array( 
					'5 Seconds'  => '5000',
					'4 Seconds'  => '4000',
					'3 Seconds'  => '3000',
					'2 Seconds'  => '4000',
					'1 Second'   => '1000',
					),
				'dependency' => array(
					'element' => 'slider_autoplay',
					'value'   => array( 'true' ),
					),
				"description" => __( "Set any value for example 5 seconds to play it in every 5 seconds. Default: 5 Seconds", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Autoplay Slide Speed", 'gymedge-core' ),
				"param_name" => "slider_autoplay_speed",
				"value" => 200,
				'dependency' => array(
					'element' => 'slider_autoplay',
					'value'   => array( 'true' ),
					),
				"description" => __( "Slide speed in milliseconds. Default: 200", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),	
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Loop", 'gymedge-core' ),
				"param_name" => "slider_loop",
				"value" => array( 
					'Enable'  => 'true',
					'Disable' => 'false',
					),
				"description" => __( "Loop to first item. Default: Enable", 'gymedge-core' ),
				"group" => __( "Slider Options", 'gymedge-core' ),
				),
			);

		return $fields;
	}

	public function shortcode( $atts, $content = '' ){
		extract( shortcode_atts( array(
			'title'                 => $this->translate['title'],
			'layout'                => 'slider',
			'slider_style'          => 'style1',
			'info_display'          => 'true',
			'slider_item_number'    => '6',
			'grid_item_number'      => '9',
			'col_lg'                => '4',
			'col_md'                => '4',
			'col_sm'                => '4',
			'col_xs'                => '6',
			'col_mobile'            => '12',
			// slider
			'slider_nav'            => 'true',
			'slider_dots'           => 'false',
			'slider_autoplay'       => 'true',
			'slider_stop_on_hover'  => 'true',
			'slider_interval'       => '5000',
			'slider_autoplay_speed' => '200',
			'slider_loop'           => 'true',
			), $atts ) );


		// validation
		$slider_item_number    = intval( $slider_item_number );
		$grid_item_number      = intval( $grid_item_number );
		$col_lg                = esc_attr( $col_lg );
		$col_md                = esc_attr( $col_md );
		$col_sm                = esc_attr( $col_sm );
		$col_xs                = esc_attr( $col_xs );
		$col_mobile            = esc_attr( $col_mobile );
		
		$owl_data = array( 
			'nav'                => ( $slider_nav === 'true' ) ? true : false,
			'navText'            => array( "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>" ),
			'dots'               => ( $slider_dots === 'true' ) ? true : false,
			'autoplay'           => ( $slider_autoplay === 'true' ) ? true: false,
			'autoplayTimeout'    => $slider_interval,
			'autoplaySpeed'      => $slider_autoplay_speed,
			'autoplayHoverPause' => ( $slider_stop_on_hover === 'true' ) ? true: false,
			'loop'               => ( $slider_loop === 'true' ) ? true: false,
			'margin'             => 20,
			'responsive'         => array(
				'0'    => array( 'items' => 12 / $col_mobile ),
				'480'  => array( 'items' => 12 / $col_xs ),
				'768'  => array( 'items' => 12 / $col_sm ),
				'992'  => array( 'items' => 12 / $col_md ),
				'1200' => array( 'items' => 12 / $col_lg ),
				)
			);

		switch ( $layout ) {
			case 'grid':
			$view = 'views/class-grid.php';
			break;
			default:
			switch ( $slider_style ) {
				case 'style2':
					$view = 'views/class-slider-2.php';
					break;				
				default:
					$owl_data['nav'] = false;
					$view = 'views/class-slider-1.php';
					break;
			}
			$this->load_scripts();
			break;
		}
		
		$owl_data = json_encode( $owl_data );
		ob_start();
		include $view;
		$output = ob_get_clean();
		return $output;
	}
}

new GymEdge_VC_Class;