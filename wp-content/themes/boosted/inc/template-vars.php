<?php
add_action( 'template_redirect', 'gymedge_template_vars' );
function gymedge_template_vars() {
	// Single Pages
	if ( is_single() || is_page() ) {
		$post_type = get_post_type();
		$post_id = get_the_id();
		switch ( $post_type ) {
			case 'page':
			$prefix = 'page';
			break;
			case 'post':
			$prefix = 'single_post';
			break;
			case 'gym_trainer':
			$prefix = 'trainer';
			break;
			case 'gym_class':
			$prefix = 'class';
			break;
			case 'product':
			$prefix = 'product';
			break;
			default:
			$prefix = 'single_post';
			break;
		}
		
		$layout          = get_post_meta( $post_id, 'gym_layout', true );
		$top_bar         = get_post_meta( $post_id, 'gym_top_bar', true );
		$header_style    = get_post_meta( $post_id, 'gym_header', true );
		$padding_top     = get_post_meta( $post_id, 'gym_top_padding', true );
		$padding_bottom  = get_post_meta( $post_id, 'gym_bottom_padding', true );
		$has_banner      = get_post_meta( $post_id, 'gym_banner', true );
		$has_breadcrumb  = get_post_meta( $post_id, 'gym_breadcrumb', true );
		$bgtype          = get_post_meta( $post_id, 'gym_banner_type', true );
		$bgcolor         = get_post_meta( $post_id, 'gym_banner_bgcolor', true );
		$bgimg           = get_post_meta( $post_id, 'gym_banner_bgimg', true );

		BooSted::$layout         = ( empty( $layout ) || $layout == 'default' ) ? BooSted::$options[$prefix. '_layout']: $layout;

		BooSted::$top_bar        = ( empty( $top_bar ) || $top_bar == 'default' ) ? BooSted::$options['top_bar']: $top_bar;

		BooSted::$header_style   = ( empty( $header_style ) || $header_style == 'default' ) ? BooSted::$options[$prefix. '_header']: $header_style;

		$padding_top               = ( empty( $padding_top ) || $padding_top == 'default' ) ? BooSted::$options[$prefix. '_padding_top']: $padding_top;
		BooSted::$padding_top    = (int) $padding_top;      

		$padding_bottom            = ( empty( $padding_bottom ) || $padding_bottom == 'default' ) ? BooSted::$options[$prefix. '_padding_bottom']: $padding_bottom;
		BooSted::$padding_bottom = (int) $padding_bottom;  

		BooSted::$has_banner     = ( empty( $has_banner ) || $has_banner == 'default' ) ? BooSted::$options[$prefix. '_banner']: $has_banner;

		BooSted::$has_breadcrumb = ( empty( $has_breadcrumb ) || $has_breadcrumb == 'default' ) ? BooSted::$options[$prefix. '_breadcrumb']: $has_breadcrumb;

		BooSted::$bgtype         = ( empty( $bgtype ) || $bgtype == 'default' ) ? BooSted::$options[$prefix. '_bgtype']: $bgtype;

		BooSted::$bgcolor        = empty( $bgcolor ) ? BooSted::$options[$prefix. '_bgcolor']: $bgcolor;

		if ( !empty( $bgimg ) ) {
			$attch_url = wp_get_attachment_image_src( $bgimg, 'boosted-size1', true );
			BooSted::$bgimg =  $attch_url[0];
		}
	elseif ( !empty( BooSted::$options[$prefix. '_bgimg']['id'] ) ) {
		$attch_url = wp_get_attachment_image_src( BooSted::$options[$prefix. '_bgimg']['id'], 'boosted-size1', true );
		BooSted::$bgimg =  $attch_url[0];
	}
	else{
		BooSted::$bgimg = BOOSTED_IMG_URL . 'banner.jpg';
	}
}

	// Blog and Archive
elseif ( is_home() || is_archive() || is_search() || is_404() ) {
	if ( is_search() ) {
		$prefix = 'search';
	}
	elseif( is_404() ){
		$prefix = 'error';
	}
	elseif( function_exists( 'is_woocommerce' ) && is_woocommerce() ){
		$prefix = 'shop';
	}
	else{
		$prefix = 'blog';
	}

	BooSted::$layout         = BooSted::$options[$prefix. '_layout'];
	BooSted::$top_bar        = BooSted::$options['top_bar'];
	BooSted::$header_style   = BooSted::$options[$prefix. '_header'];
	BooSted::$padding_top    = BooSted::$options[$prefix. '_padding_top'];
	BooSted::$padding_bottom = BooSted::$options[$prefix. '_padding_bottom'];
	BooSted::$has_banner     = BooSted::$options[$prefix. '_banner'];
	BooSted::$has_breadcrumb = BooSted::$options[$prefix. '_breadcrumb'];
	BooSted::$bgtype         = BooSted::$options[$prefix. '_bgtype'];
	BooSted::$bgcolor        = BooSted::$options[$prefix. '_bgcolor'];

	if ( !empty( BooSted::$options[$prefix. '_bgimg']['id'] ) ) {
		$attch_url = wp_get_attachment_image_src( BooSted::$options[$prefix. '_bgimg']['id'], 'boosted-size1', true );
		BooSted::$bgimg =  $attch_url[0];
	}
	else{
		BooSted::$bgimg = BOOSTED_IMG_URL . 'banner.jpg';
	}		
}
}

// Add body class
add_filter( 'body_class' , 'gymedge_body_classes' );
function gymedge_body_classes( $classes ) {
	$classes[] = 'non-stick';
	if ( BooSted::$header_style == 'st2' ) {
		$classes[] = 'header-style-2';
	}
	$classes[] = ( BooSted::$layout == 'full-width' ) ? 'no-sidebar' : 'has-sidebar';

	if ( isset( $_COOKIE["shopview"] ) && $_COOKIE["shopview"] == 'list' ) {
		$classes[] = 'product-list-view';
	}
	else {
		$classes[] = 'product-grid-view';
	}

	return $classes;
}