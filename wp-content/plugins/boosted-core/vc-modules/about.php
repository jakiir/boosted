<?php
class GymEdge_VC_About extends GymEdge_VC_Modules {

	public $translate;

	public function __construct(){
		$this->name = __( "GymEdge: About", 'gymedge-core' );
		$this->base = 'gymedge-vc-about';
		$this->translate = array(
			'title'      => __( 'All About', 'gymedge-core' ),
			'subtitle'   => __( '<span class="gym-primary-color">GYM</span> FITNESS', 'gymedge-core' ),
			'buttontext' => __( "SIGN UP", 'gymedge-core' ),
		);
		parent::__construct();
	}

	public function fields(){
		$fields = array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Title", 'gymedge-core' ),
				"param_name" => "title",
				"value" => $this->translate['title'],
			),
			array(
				"type" => "textarea_raw_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Subtitle", 'gymedge-core' ),
				"param_name" => "subtitle",
				"value" => base64_encode( $this->translate['subtitle'] ),
				"rows" => "1",
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Description", 'gymedge-core' ),
				"param_name" => "content",
				"value" => __( "I am Info Text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.", 'gymedge-core' ),
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Button Text", 'gymedge-core' ),
				"param_name" => "buttontext",
				"value" => $this->translate['buttontext'],
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Button URL", 'gymedge-core' ),
				"param_name" => "buttonurl",
			),
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Image", 'gymedge-core' ),
				"param_name" => "image",
			),
		);
		return $fields;
	}

	public function shortcode( $atts, $content = '' ){
		extract( shortcode_atts( array(
			'title'      => $this->translate['title'],
			'subtitle'   => base64_encode( $this->translate['subtitle'] ),
			'buttontext' => $this->translate['buttontext'],
			'buttonurl'  => '',
			'image'      => '',
			), $atts ) );

		ob_start();
		include 'views/about-1.php';
		$output = ob_get_clean();
		return $output;
	}
}

new GymEdge_VC_About;