<?php
if (!isset($content_width)) {
	$content_width = 1200;
}

add_action('after_setup_theme', 'gymedge_setup');
function gymedge_setup() {
	// Language
	load_theme_textdomain('boosted', BOOSTED_BASE_DIR . 'languages');

	// Theme support
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links');
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
	add_theme_support('woocommerce');

	// Image sizes
	add_image_size( 'boosted-size1', 1200, 600, true ); // post large
	add_image_size( 'boosted-size2', 410, 200, true ); // post small, class slider
	add_image_size( 'boosted-size3', 360, 460, true ); // trainer
	add_image_size( 'boosted-size4', 360, 360, array( 'center', 'top' ) ); // trainer squire
	add_image_size( 'boosted-size5', 360, 300, true ); // class grid
	add_image_size( 'boosted-size6', 800, 600, true ); // gallery large
	add_image_size( 'boosted-size7', 400, 270, true ); // gallery small

	// Register menus
	register_nav_menus(array(
		'primary' => esc_html__('Primary', 'boosted'),
		'top' => esc_html__('Header Top', 'boosted'),
	));
}