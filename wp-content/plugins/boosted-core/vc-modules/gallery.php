<?php
class GymEdge_VC_Gellery extends GymEdge_VC_Modules {

	public function __construct(){
		$this->name = __( "GymEdge: Gallery", 'gymedge-core' );
		$this->base = 'gymedge-vc-gallery';
		$this->translate = array(
			'title' => __( "OUR GALLERY", 'gymedge-core' ),
			'all'   => __( "All", 'gymedge-core' ),
		);
		parent::__construct();
	}

	public function load_scripts(){
		wp_enqueue_style( 'magnific-popup' );
		wp_enqueue_script( 'isotope-pkgd' );
		wp_enqueue_script( 'jquery-magnific-popup' );
		wp_enqueue_script( 'gym-vc-gallery' );
	}

	public function fields(){
		$fields = array(
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Style", 'gymedge-core' ),
				"param_name" => "style",
				"value" => array( 
					'Style 1' => 'style1',
					'Style 2' => 'style2',
					),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Title", 'gymedge-core' ),
				"param_name" => "title",
				"value" => $this->translate['title'],
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'style1' ),
					),
				),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "All items name", 'gymedge-core' ),
				"param_name" => "all",
				'value' => $this->translate['all'],
				),
			);
		return $fields;
	}

	public function shortcode( $atts, $content = '' ){
		extract( shortcode_atts( array(
			'style' => 'style1',
			'title' => $this->translate['title'],
			'all'   => $this->translate['all'],
			), $atts ) );

		$this->load_scripts();
		ob_start();
		include 'views/gallery-1.php';
		$output = ob_get_clean();
		return $output;
	}
}

new GymEdge_VC_Gellery;