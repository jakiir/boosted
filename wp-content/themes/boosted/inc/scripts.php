<?php
function gymedge_fonts_url(){
	$fonts_url = '';
	if ( 'off' !== _x( 'on', 'Google fonts - Open Sans and Roboto : on or off', 'boosted' ) ) {
		$fonts_url = add_query_arg( 'family', urlencode( 'Open Sans:400,400i,600|Roboto:400,500,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
	}
	return $fonts_url;
}

add_action( 'wp_enqueue_scripts', 'gymedge_register_scripts', 12 );
function gymedge_register_scripts(){
	/*CSS*/
	// owl.carousel CSS
	wp_register_style( 'owl-carousel',       BOOSTED_CSS_URL . 'owl.carousel.min.css', array(), BOOSTED_THEME_VERSION );
	wp_register_style( 'owl-theme-default',  BOOSTED_CSS_URL . 'owl.theme.default.min.css', array(), BOOSTED_THEME_VERSION );
	// magnific popup
	wp_register_style( 'magnific-popup',     BOOSTED_CSS_URL . 'magnific-popup.min.css', array(), BOOSTED_THEME_VERSION );

	/*JS*/
	// owl.carousel.min js
	wp_register_script( 'owl-carousel',          BOOSTED_JS_URL . 'owl.carousel.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// isotope
	wp_register_script( 'isotope-pkgd',          BOOSTED_JS_URL . 'isotope.pkgd.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// magnific popup
	wp_register_script( 'jquery-magnific-popup', BOOSTED_JS_URL . 'jquery.magnific-popup.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// vc gallery js
	wp_register_script( 'bosd-vc-gallery',        BOOSTED_JS_URL . 'vc-gallery.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );

}

add_action( 'wp_enqueue_scripts', 'gymedge_enqueue_scripts', 15 );
function gymedge_enqueue_scripts() {
	/*CSS*/
	// Google fonts
	wp_enqueue_style( 'boosted-gfonts',       gymedge_fonts_url(), array(), BOOSTED_THEME_VERSION );
	// Bootstrap CSS
	wp_enqueue_style( 'bootstrap',            BOOSTED_CSS_URL . 'bootstrap.min.css', array(), BOOSTED_THEME_VERSION );
	// font-awesome CSS
	wp_enqueue_style( 'font-awesome',         BOOSTED_CSS_URL . 'font-awesome.min.css', array(), BOOSTED_THEME_VERSION );
	// Meanmenu CSS
	wp_enqueue_style( 'meanmenu',             BOOSTED_CSS_URL . 'meanmenu.css', array(), BOOSTED_THEME_VERSION );
	// main CSS
	wp_enqueue_style( 'boosted-default',      BOOSTED_CSS_URL . 'default.css', array(), BOOSTED_THEME_VERSION );
	// vc modules css
	wp_enqueue_style( 'boosted-vc',           BOOSTED_CSS_URL . 'vc.css', array(), BOOSTED_THEME_VERSION );
	// style CSS
	wp_enqueue_style( 'boosted-style',        BOOSTED_CSS_URL . 'style.css', array(), BOOSTED_THEME_VERSION );
	// responsive CSS
	wp_enqueue_style( 'boosted-responsive',   BOOSTED_CSS_URL . 'responsive.css', array(), BOOSTED_THEME_VERSION );
	// variable style CSS
	ob_start();
	include BOOSTED_INC_DIR . 'variable-style.php';
	include BOOSTED_INC_DIR . 'variable-style-vc.php';
	$variable_css  = ob_get_clean();
	$variable_css .= wp_kses_post( BooSted::$options['custom_css'] ); // custom css
	wp_add_inline_style( 'boosted-responsive', $variable_css );

	/*JS*/
	// bootstrap js
	wp_enqueue_script( 'bootstrap',             BOOSTED_JS_URL . 'bootstrap.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// Nav smooth scroll
	wp_enqueue_script( 'jquery-nav',            BOOSTED_JS_URL . 'jquery.nav.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// Meanmenu js
	wp_enqueue_script( 'jquery-meanmenu',       BOOSTED_JS_URL . 'jquery.meanmenu.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// Comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// Cookie js
	wp_enqueue_script( 'js-cookie',             BOOSTED_JS_URL . 'js.cookie.min.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// main js
	wp_enqueue_script( 'boosted-main',          BOOSTED_JS_URL . 'main.js', array( 'jquery' ), BOOSTED_THEME_VERSION, true );
	// localize script
	$gym_localize_data = array(
		'stickyMenu' => BooSted::$options['sticky_menu'],
		'siteLogo'   => '<a href="' . esc_url( home_url( '/' ) ) . '" alt="' . esc_attr( get_bloginfo('title') ) . '"><img class="logo-small" src="'. esc_url( BooSted::$options['logo']['url'] ).'" /></a>',
	);
	wp_localize_script( 'boosted-main', 'gymEdgeObj', $gym_localize_data );
}

// Admin Scripts
add_action( 'admin_enqueue_scripts', 'gymedge_admin_scripts', 12 );
function gymedge_admin_scripts(){
	global $pagenow, $typenow;

	wp_enqueue_style( 'font-awesome', BOOSTED_CSS_URL . 'font-awesome.min.css', array(), BOOSTED_THEME_VERSION );

	if( !in_array( $pagenow, array('post.php', 'post-new.php', 'edit.php') ) ) return;
	
	if( $typenow == 'wlshowcasesc' ){
		wp_enqueue_style( 'boosted-logo-showcase', BOOSTED_CSS_URL . 'admin-logo-showcase.css', array(), BOOSTED_THEME_VERSION );
	}
}