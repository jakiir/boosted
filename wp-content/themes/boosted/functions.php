<?php
/**
 * boosted functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package boosted
 */

if ( ! function_exists( 'boosted_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function boosted_setup() {

		$bosd_theme_data = wp_get_theme();
		define( 'BOOSTED_THEME_VERSION', ( WP_DEBUG ) ? time() : $bosd_theme_data->get( 'Version' ) );
		define( 'BOOSTED_THEME_AUTHOR_URI', $bosd_theme_data->get( 'AuthorURI' ) );
		
		// DIR
		define( 'BOOSTED_BASE_DIR',    get_template_directory(). '/' );
		define( 'BOOSTED_INC_DIR',     BOOSTED_BASE_DIR . 'inc/' );
		define( 'BOOSTED_LIB_DIR',     BOOSTED_BASE_DIR . 'lib/' );
		define( 'BOOSTED_WID_DIR',     BOOSTED_INC_DIR . 'widgets/' );
		define( 'BOOSTED_PLUGINS_DIR', BOOSTED_INC_DIR . 'plugins/' );
		define( 'BOOSTED_JS_DIR',      BOOSTED_BASE_DIR . 'assets/js/' );

		// URL
		define( 'BOOSTED_BASE_URL',    get_template_directory_uri(). '/' );
		define( 'BOOSTED_ASSETS_URL',  BOOSTED_BASE_URL . 'assets/' );
		define( 'BOOSTED_CSS_URL',     BOOSTED_ASSETS_URL . 'css/' );
		define( 'BOOSTED_JS_URL',      BOOSTED_ASSETS_URL . 'js/' );
		define( 'BOOSTED_IMG_URL',     BOOSTED_ASSETS_URL . 'img/' );

		// Includes
		require_once BOOSTED_INC_DIR . 'redux-config.php';
		// Includes
		require_once BOOSTED_INC_DIR . 'redux-config.php';
		require_once BOOSTED_INC_DIR . 'boosted.php';
		require_once BOOSTED_INC_DIR . 'helper-functions.php';
		require_once BOOSTED_INC_DIR . 'general.php';
		require_once BOOSTED_INC_DIR . 'scripts.php';
		require_once BOOSTED_INC_DIR . 'template-vars.php';
		require_once BOOSTED_INC_DIR . 'woo-hooks.php';
		require_once BOOSTED_INC_DIR . 'vc-settings.php';


		// Widgets
		require_once BOOSTED_WID_DIR . 'widget-settings.php';
		require_once BOOSTED_WID_DIR . 'rt-widget-fields.php';
		require_once BOOSTED_WID_DIR . 'address-widget.php';
		require_once BOOSTED_WID_DIR . 'about-widget.php';
		require_once BOOSTED_WID_DIR . 'search-widget.php'; // override default


		// TGM Plugin Activation
		if ( is_admin() ) {
			require_once BOOSTED_LIB_DIR . 'class-tgm-plugin-activation.php';
			require_once BOOSTED_INC_DIR . 'tgm-config.php';
		}

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on boosted, use a find and replace
		 * to change 'boosted' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'boosted', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'boosted' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'boosted_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'boosted_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function boosted_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'boosted_content_width', 640 );
}
add_action( 'after_setup_theme', 'boosted_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function boosted_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'boosted' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'boosted' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'boosted_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function boosted_scripts() {
	wp_enqueue_style( 'boosted-style', get_stylesheet_uri() );

	wp_enqueue_script( 'boosted-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'boosted-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'boosted_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

