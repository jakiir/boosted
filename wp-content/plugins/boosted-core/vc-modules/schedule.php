<?php
class GymEdge_VC_Schedule extends GymEdge_VC_Modules {

	public function __construct(){
		$this->name = __( "GymEdge: Schedule", 'gymedge-core' );
		$this->base = 'gymedge-vc-schedule';
		parent::__construct();
	}

	public function fields(){
		$fields = array(
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Schedule By", 'gymedge-core' ),
				"param_name" => "schedule_by",
				"value" => array( 
					'Weekday'  => 'weekday',
					'Class' => 'class',
					),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Background", 'gymedge-core' ),
				"param_name" => "schedule_background",
				"value" => array( 
					'Disabled' => 'false',
					'Enabled'  => 'true',
					),
				),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Button", 'gymedge-core' ),
				"param_name" => "schedule_button",
				"value" => array( 
					'Enabled'  => 'true',
					'Disabled' => 'false',
					),
				),
			);
		return $fields;
	}

	public function shortcode( $atts, $content = '' ){
		extract( shortcode_atts( array(
			'schedule_background'  => 'false',
			'schedule_button'      => 'true',
			'schedule_by'          => 'weekday',
			), $atts ) );

		// css classes
		$schedule_class  = '';
		$schedule_class .= ( $schedule_background == 'false' ) ? ' schedule-no-background' : ' schedule-has-background';
		$schedule_class .= ( $schedule_button == 'false' ) ? ' schedule-no-button' : ' schedule-has-button';

		// week names array
		$weeknames = array(
			'mon' => __( 'Monday', 'gymedge-core' ),
			'tue' => __( 'Tuesday', 'gymedge-core' ),
			'wed' => __( 'Wednesday', 'gymedge-core' ),
			'thu' => __( 'Thursday', 'gymedge-core' ),
			'fri' => __( 'Friday', 'gymedge-core' ),
			'sat' => __( 'Saturday', 'gymedge-core' ),
			'sun' => __( 'Sunday', 'gymedge-core' ),
			);

		// class post types array
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'gym_class',
			);
		$classes = get_posts( $args );
		$uniqid = (int) rand();

		ob_start();
		switch ( $schedule_by ) {
			case 'weekday':
				include 'views/schedule-by-week.php';
				break;
			default:
				include 'views/schedule-by-class.php';
				break;
		}

		$output = ob_get_clean();
		return $output;
	}
}

new GymEdge_VC_Schedule;