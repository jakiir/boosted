<?php
add_action( 'tgmpa_register', 'boosted_register_required_plugins' );
function boosted_register_required_plugins() {
	$plugins = array(
		// Bundled
		array(
			'name'         => 'Boosted Core',
			'slug'         => 'boosted-core',
			'source'       => 'boosted-core.zip',
			'required'     =>  true,
			'external_url' => 'http://radiustheme.com',
			'version'      => '1.3.1'
		),
		array(
			'name'         => 'Demo Importer',
			'slug'         => 'rt-demo-importer',
			'source'       => 'rt-demo-importer.zip',
			'required'     =>  true,
			'external_url' => 'http://radiustheme.com',
			'version'      => '1.0.0'
		),
		array(
			'name'         => 'WPBakery Visual Composer',
			'slug'         => 'js_composer',
			'source'       => 'js_composer.zip',
			'required'     => true,
			'external_url' => 'http://vc.wpbakery.com',
			'version'      => '5.0.1'
		),
		array(
			'name'         => 'LayerSlider WP',
			'slug'         => 'LayerSlider',
			'source'       => 'LayerSlider.zip',
			'required'     => false,
			'external_url' => 'https://layerslider.kreaturamedia.com',
			'version'      => '6.1.6'
		),
		array(
			'name'         => 'CMB2',
			'slug'         => 'cmb2',
			'source'       => 'cmb2.zip',
			'required'     => false,
			'external_url' => 'https://radiustheme.com/',
			'version'      => '2.1.1'
		),

		// Repository
		array(
			'name'     => 'Redux Framework',
			'slug'     => 'redux-framework',
			'required' => true,
		),
		array(
			'name'     => 'Breadcrumb NavXT',
			'slug'     => 'breadcrumb-navxt',
			'required' => true,
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => false,
		),
		array(
			'name'     => 'WP Extended Search',
			'slug'     => 'wp-extended-search',
			'required' => false,
		),
		array(
			'name'     => 'Easy Twitter Feed Widget',
			'slug'     => 'easy-twitter-feed-widget',
			'required' => false,
		),
		array(
			'name'     => 'Flickr Badges Widget',
			'slug'     => 'flickr-badges-widget',
			'required' => false,
		),
		array(
			'name'     => 'WP Retina 2x',
			'slug'     => 'wp-retina-2x',
			'required' => false,
		),
		array(
			'name'     => 'WooCommerce',
			'slug'     => 'woocommerce',
			'required' => false,
		),
		array(
			'name'     => 'YITH WooCommerce Quick View',
			'slug'     => 'yith-woocommerce-quick-view',
			'required' => false,
		),
		array(
			'name'     => 'YITH WooCommerce Wishlist',
			'slug'     => 'yith-woocommerce-wishlist',
			'required' => false,
		),

	);

	$config = array(
		'id'           => 'boosted',             // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => BOOSTED_PLUGINS_DIR,   // Default absolute path to bundled plugins.
		'menu'         => 'boosted-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
